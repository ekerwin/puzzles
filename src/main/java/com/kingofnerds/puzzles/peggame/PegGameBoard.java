package com.kingofnerds.puzzles.peggame;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
public class PegGameBoard {
    private final PegGameMoves pegGameMoves;

    private final Set<PegGameMove> legalMoves = new HashSet<>();
    private final Set<Integer> pegPositions = new HashSet<>();
    private final Set<Integer> emptyPositions = new HashSet<>();
    private int pegCount;

    public PegGameBoard(final PegGameMoves pegGameMoves) {
        this.pegGameMoves = pegGameMoves;
        for (int positionIndex = 1; positionIndex <= 15; positionIndex++) {
            pegPositions.add(positionIndex);
        }
        pegCount = pegPositions.size();
    }

    public PegGameBoard(final PegGameBoard original) {
        pegGameMoves = original.pegGameMoves;
        legalMoves.addAll(original.legalMoves);
        pegPositions.addAll(original.pegPositions);
        emptyPositions.addAll(original.emptyPositions);
        pegCount = original.pegCount;
    }

    public PegGameBoard removePegs(final Set<Integer> pegsToRemove) {
        final PegGameBoard pegGameBoard = new PegGameBoard(this);
        for (final Integer pegPosition : pegsToRemove) {
            pegGameBoard.pegPositions.remove(pegPosition);
            pegGameBoard.emptyPositions.add(pegPosition);
        }

        pegGameBoard.pegCount = pegGameBoard.pegPositions.size();
        pegGameBoard.updateLegalMoves();

        return pegGameBoard;
    }

    public PegGameBoard movePegs(final PegGameMove move) {
        final PegGameBoard pegGameBoard = new PegGameBoard(this);

        pegGameBoard.pegPositions.remove(move.getSource());
        pegGameBoard.pegPositions.remove(move.getJumped());
        pegGameBoard.pegPositions.add(move.getTarget());

        pegGameBoard.emptyPositions.add(move.getSource());
        pegGameBoard.emptyPositions.add(move.getJumped());
        pegGameBoard.emptyPositions.remove(move.getTarget());

        pegGameBoard.pegCount--;
        pegGameBoard.updateLegalMoves();

        return pegGameBoard;
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append(positionString(1));
        builder.append(positionString(2, 3));
        builder.append(positionString(4, 5, 6));
        builder.append(positionString(7, 8, 9, 10));
        builder.append(positionString(11, 12, 13, 14, 15));

        return builder.toString();
    }

    private void updateLegalMoves() {
        legalMoves.clear();
        for (final Integer pegPosition : pegPositions) {
            final Set<PegGameMove> possibleMovesFromPosition = pegGameMoves.getMovesFromPosition(pegPosition);
            for (final PegGameMove move : possibleMovesFromPosition) {
                if (pegPositions.contains(move.getJumped()) && emptyPositions.contains(move.getTarget())) {
                    legalMoves.add(move);
                }
            }
        }
    }

    private String positionString(final int... positions) {
        final StringBuilder builder = new StringBuilder();
        builder.append(StringUtils.repeat(" ", 8 - positions.length));

        final List<String> stateStrings = new ArrayList<>();
        for (final int positionIndex : positions) {
            if (pegPositions.contains(positionIndex)) {
                stateStrings.add("O");
            } else {
                stateStrings.add("-");
            }
        }

        builder.append(StringUtils.join(stateStrings, " "));
        builder.append(StringUtils.repeat(" ", 8 - positions.length));
        builder.append("\n");

        return builder.toString();
    }

}
