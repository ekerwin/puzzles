package com.kingofnerds.puzzles.peggame;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class PegGameMoves {
    private final Set<PegGameMove> possibleMoves = new HashSet<>();
    private final Map<Integer, Set<PegGameMove>> movesFromPosition = new HashMap<>();

    public PegGameMoves() {
        addPossibleMove(new PegGameMove(1, 2, 4));
        addPossibleMove(new PegGameMove(1, 3, 6));

        addPossibleMove(new PegGameMove(2, 4, 7));
        addPossibleMove(new PegGameMove(2, 5, 9));

        addPossibleMove(new PegGameMove(3, 5, 8));
        addPossibleMove(new PegGameMove(3, 6, 10));

        addPossibleMove(new PegGameMove(4, 2, 1));
        addPossibleMove(new PegGameMove(4, 5, 6));
        addPossibleMove(new PegGameMove(4, 7, 11));
        addPossibleMove(new PegGameMove(4, 8, 13));

        addPossibleMove(new PegGameMove(5, 8, 12));
        addPossibleMove(new PegGameMove(5, 9, 14));

        addPossibleMove(new PegGameMove(6, 3, 1));
        addPossibleMove(new PegGameMove(6, 5, 4));
        addPossibleMove(new PegGameMove(6, 9, 13));
        addPossibleMove(new PegGameMove(6, 10, 15));

        addPossibleMove(new PegGameMove(7, 4, 2));
        addPossibleMove(new PegGameMove(7, 8, 9));

        addPossibleMove(new PegGameMove(8, 5, 3));
        addPossibleMove(new PegGameMove(8, 9, 10));

        addPossibleMove(new PegGameMove(9, 5, 2));
        addPossibleMove(new PegGameMove(9, 8, 7));

        addPossibleMove(new PegGameMove(10, 6, 3));
        addPossibleMove(new PegGameMove(10, 9, 8));

        addPossibleMove(new PegGameMove(11, 7, 4));
        addPossibleMove(new PegGameMove(11, 12, 13));

        addPossibleMove(new PegGameMove(12, 8, 5));
        addPossibleMove(new PegGameMove(12, 13, 14));

        addPossibleMove(new PegGameMove(13, 8, 4));
        addPossibleMove(new PegGameMove(13, 9, 6));

        addPossibleMove(new PegGameMove(14, 9, 5));
        addPossibleMove(new PegGameMove(14, 13, 12));

        addPossibleMove(new PegGameMove(15, 10, 6));
        addPossibleMove(new PegGameMove(15, 14, 13));
    }

    public boolean isMovePossible(final PegGameMove move) {
        return possibleMoves.contains(move);
    }

    public Set<PegGameMove> getMovesFromPosition(final int position) {
        return movesFromPosition.get(position);
    }

    private void addPossibleMove(final PegGameMove move) {
        possibleMoves.add(move);

        if (!movesFromPosition.containsKey(move.getSource())) {
            movesFromPosition.put(move.getSource(), new HashSet<>());
        }
        movesFromPosition.get(move.getSource()).add(move);
    }

}
