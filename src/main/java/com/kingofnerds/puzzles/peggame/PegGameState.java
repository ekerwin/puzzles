package com.kingofnerds.puzzles.peggame;

import java.util.List;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Getter
@RequiredArgsConstructor
@EqualsAndHashCode
@ToString
public class PegGameState {
    private final PegGameBoard originalBoard;
    private final PegGameBoard currentBoard;
    private final List<PegGameMove> movesToTransformOriginalToCurrent;

}
