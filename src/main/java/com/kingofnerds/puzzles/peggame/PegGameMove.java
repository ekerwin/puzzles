package com.kingofnerds.puzzles.peggame;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@EqualsAndHashCode
@ToString
public class PegGameMove {
    private final int source;
    private final int jumped;
    private final int target;

    public PegGameMove(final int source, final int jumped, final int target) {
        this.source = source;
        this.jumped = jumped;
        this.target = target;
    }

}
