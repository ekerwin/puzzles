package com.kingofnerds.puzzles.peggame;

import java.util.ArrayList;
import java.util.List;

import com.kingofnerds.puzzles.peggame.solution.PegGameSolution;

public class PegGameSolutionFinder {
    public List<PegGameState> solve(final PegGameBoard board, final PegGameSolution solution) {
        List<PegGameState> pegGameStates = new ArrayList<>();
        pegGameStates.add(new PegGameState(board, board, new ArrayList<>()));

        boolean changedState = false;
        do {
            changedState = false;
            final List<PegGameState> pegGameStatesCopy = new ArrayList<>(pegGameStates);
            pegGameStates = new ArrayList<>();
            for (final PegGameState pegGameState : pegGameStatesCopy) {
                if (canContinue(pegGameState, solution)) {
                    changedState = true;
                    final List<PegGameState> nextPegGameStates = getNextPegGameStates(pegGameState);
                    pegGameStates.addAll(nextPegGameStates);
                } else if (solution.isSolved(pegGameState.getCurrentBoard())) {
                    pegGameStates.add(pegGameState);
                }
            }
        } while (changedState);

        return pegGameStates;
    }

    public boolean canContinue(final PegGameState pegGameState, final PegGameSolution solution) {
        return pegGameState.getCurrentBoard().getLegalMoves().size() > 0 && !solution.isSolved(pegGameState.getCurrentBoard());
    }

    public List<PegGameState> getNextPegGameStates(final PegGameState previousPegGameState) {
        final List<PegGameState> nextPegGameStates = new ArrayList<>();
        for (final PegGameMove move : previousPegGameState.getCurrentBoard().getLegalMoves()) {
            final PegGameBoard changedBoard = previousPegGameState.getCurrentBoard().movePegs(move);
            final List<PegGameMove> previousMoves = previousPegGameState.getMovesToTransformOriginalToCurrent();
            final List<PegGameMove> nextMoves = new ArrayList<>(previousMoves);
            nextMoves.add(move);
            nextPegGameStates.add(new PegGameState(previousPegGameState.getOriginalBoard(), changedBoard, nextMoves));
        }

        return nextPegGameStates;
    }

}
