package com.kingofnerds.puzzles.peggame.solution;

import com.kingofnerds.puzzles.peggame.PegGameBoard;

public class EightPegsLeftNoLegalMovesSolution implements PegGameSolution {
    @Override
    public boolean isSolved(final PegGameBoard board) {
        return board.getPegCount() == 8 && board.getLegalMoves().size() == 0;
    }

}
