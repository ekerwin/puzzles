package com.kingofnerds.puzzles.peggame.solution;

import com.kingofnerds.puzzles.peggame.PegGameBoard;

public interface PegGameSolution {
    public boolean isSolved(PegGameBoard board);

}
