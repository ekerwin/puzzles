package com.kingofnerds.puzzles.peggame.solution;

import com.kingofnerds.puzzles.peggame.PegGameBoard;

public class OnePegLeftInOriginalHoleSolution implements PegGameSolution {
    int originalHolePosition;

    public OnePegLeftInOriginalHoleSolution(final int originalHolePosition) {
        this.originalHolePosition = originalHolePosition;
    }

    @Override
    public boolean isSolved(final PegGameBoard board) {
        return board.getPegCount() == 1 && board.getPegPositions().contains(originalHolePosition);
    }

}
