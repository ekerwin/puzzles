package com.kingofnerds.puzzles.codefight;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * // @formatter:off
 * The difference between two sequences of the same length a1, a2, a3,..., an
 * and b1, b2, b3,..., bn can be defined as the sum of absolute differences
 * between their respective elements:
 *
 * diff(a, b) = |a1 - b1| + |a2 - b2| + ... + |an - bn|.
 *
 * For the given sequences a and b (not necessarily having the same lengths)
 * find a subsequence b' of b such that diff(a, b') is minimal. Return this
 * difference.
 *
 * Example
 *
 * For a = [1, 2, 6] and b = [0, 1, 3, 4, 5], the output should be
 * closestSequence2(a, b) = 2.
 *
 * The best subsequence will be b' = [1, 3, 5] which has a difference of 2 with
 * a.
 *
 * [time limit] 3000ms (java)
 * [input] array.integer a
 * [input] array.integer b
 * [output] integer
 *
 * Constraints:
 * 3 ≤ a.length ≤ 1000
 * -1000 ≤ a[i] ≤ 1000
 * a.length ≤ b.length ≤ 1000
 * -1000 ≤ b[i] ≤ 1000
 *
 * // @formatter:on
 */
public class ClosestSequence2 {
    public static void main(final String[] args) {
        // 4
        final int[] a = new int[] { 1, 3, 5 };
        final int[] b = new int[] { 5, 3, 1, 5, 3, 0 };
        final ClosestSequence2 closestSequence = new ClosestSequence2();
        final int difference = closestSequence.closestSequence2(a, b);
        System.out.println(difference);

        // 7
        final int[] a2 = new int[] { 1, 2, 1, 2, 1, 2 };
        final int[] b2 = new int[] { 3, 0, 0, 3, 0, 3, 3, 0, 0 };
        final int difference2 = closestSequence.closestSequence2(a2, b2);
        System.out.println(difference2);
    }

    public static Map<String, Integer> computedDiffs = new HashMap<>();

    public int closestSequence2(final int[] a, final int[] b) {
        final String key = join(a) + "|" + join(b);
        if (computedDiffs.containsKey(key)) {
            return computedDiffs.get(key);
        }

        int difference = 0;
        if (a.length == 1) {
            difference = simpleDiff(a[0], b);
        } else if (a.length == b.length) {
            difference = simpleDiff(a, b);
        } else {
            final int x = Math.abs(a[0] - b[0]) + closestSequence2(Arrays.copyOfRange(a, 1, a.length), Arrays.copyOfRange(b, 1, b.length));
            final int y = closestSequence2(a, Arrays.copyOfRange(b, 1, b.length));
            difference = Math.min(x, y);
        }
        computedDiffs.put(key, difference);
        return difference;
    }

    public int simpleDiff(final int a, final int[] b) {
        int minimumDifference = Integer.MAX_VALUE;
        for (final int bValue : b) {
            minimumDifference = Math.min(minimumDifference, Math.abs(a - bValue));
        }

        return minimumDifference;
    }

    public int simpleDiff(final int a[], final int b[]) {
        int simpleDiff = 0;
        for (int index = 0; index < a.length; index++) {
            simpleDiff += Math.abs(a[index] - b[index]);
        }
        return simpleDiff;
    }

    public String join(final int[] array) {
        final StringBuilder b = new StringBuilder();
        for (int i = 0; i < array.length; i++) {
            if (i > 0) {
                b.append(",");
            }
            b.append(array[i]);
        }
        return b.toString();
    }

}
