package com.kingofnerds.puzzles.codefight;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
@formatter:off
Cirno hates math. She is the worst math student that has ever studied in her school. But Cirno loves anime, and is very interested in those "most moe" competitions.

She has recently found out that one of the competition websites started to use math equation captchas to prevent mass automatic voting. The captcha represents a linear math equation (with the usual distorted-background obfuscation) with one unknown x, and asks the voter to type in the solution of the equation.

That site also provides a link to an online equation solver, so at first Cirno happily used this solver to vote. But after some time going to the online solver became irritating, as the loading time of the solver was way too long. She asks you to write her a personal solver.

She found out that the equation the captcha gives has the following properties:

The equation only involves one unknown, and is always x.

The equation can only take one of the following form:

[-]Nx[+-]M=[-]K
[-]M[+-]Nx=[-]K
[-]K=[-]Nx[+-]M
[-]K=[-]M[+-]Nx
Here, N, M, K are positive integers, N will be omitted if it is 1, [-] means a possible negative sign, [+-] means either a plus sign or a minus sign, and = and x are verbatim.

The solution is always unique and is an integer.

Help her with your program!

Example

For equation = "3x+4=7", the answer will be 1.
For equation = "5+x=9", the answer will be 4.
For equation = "-44=7x-2", the answer will be -6.
For equation = "-8=-21-x", the answer will be -13.
These four examples are the samples for the four possible forms of the equation respectively.

Input/Output

[time limit] 3000ms (java)
[input] string equation

The equation given as described above.

Constraint:
The integers N, M, K of the equation are guaranteed to be less than 1000.

[output] integer

The value of x found from the equation.
The solution is guaranteed to be unique and is an integer.
@formatter:on
 */
public class EquationCaptcha {
    public static void main(final String[] args) {
        final EquationCaptcha captcha = new EquationCaptcha();

        String equation = "3x+4=7";
        System.out.println(equation + ", 1, " + captcha.equationCaptcha(equation));
        equation = "5+x=9";
        System.out.println(equation + ", 4, " + captcha.equationCaptcha(equation));
        equation = "-44=7x-2";
        System.out.println(equation + ", -6, " + captcha.equationCaptcha(equation));
        equation = "-8=-21-x";
        System.out.println(equation + ", -13, " + captcha.equationCaptcha(equation));
    }

    public int equationCaptcha(final String equation) {
        final String[] pieces = equation.split("=");
        String leftHandSide = pieces[0];
        String rightHandSide = pieces[1];
        if (rightHandSide.contains("x")) {
            final String temp = rightHandSide;
            rightHandSide = leftHandSide;
            leftHandSide = temp;
        }

        // System.out.println(leftHandSide);
        // System.out.println(rightHandSide);
        splitVariables(leftHandSide);
        System.out.println("\n\n");
        return 0;
    }

    /**
     * [-]Nx[+-]M=[-]K [-]M[+-]Nx=[-]K [-]K=[-]Nx[+-]M [-]K=[-]M[+-]Nx
     */
    public String[] splitVariables(final String expression) {
        final Pattern pattern = Pattern.compile("((\\-|\\+)?[0-9]*x)");
        final Matcher matcher = pattern.matcher(expression);
        if (matcher.find()) {
            final String xPart = matcher.group();
            System.out.println(xPart);
            // System.out.println(expression.replace(xPart, ""));
        }

        return new String[] { "" };
    }

}
