package com.kingofnerds.puzzles.codefight;

public class AlternatingKeys {
    public static void main(final String[] args) {
        final AlternatingKeys alternatingKeys = new AlternatingKeys();
        System.out.println(alternatingKeys.alternatingKeys("abc") == 200);
        System.out.println(alternatingKeys.alternatingKeys("   ") == 150);
        System.out.println(alternatingKeys.alternatingKeys("aa") == 125);
        System.out.println(alternatingKeys.alternatingKeys(" am") == 150);
        System.out.println(alternatingKeys.alternatingKeys(" ma") == 150);
        System.out.println(alternatingKeys.alternatingKeys("a  a") == 225);
        System.out.println(alternatingKeys.alternatingKeys("a   a") == 250);
        System.out.println(alternatingKeys.alternatingKeys("ak") == 100);
        System.out.println(alternatingKeys.alternatingKeys(" ") == 50);
        System.out.println(alternatingKeys.alternatingKeys("land") == 200);
        System.out.println(alternatingKeys.alternatingKeys("a b c") == 250);
        System.out.println(alternatingKeys.alternatingKeys("a l") == 175);
        System.out.println(alternatingKeys.alternatingKeys("a a") == 150);
        System.out.println(alternatingKeys.alternatingKeys("a a a") == 250);
        System.out.println(alternatingKeys.alternatingKeys("a l a l a") == 550);
        System.out.println(alternatingKeys.alternatingKeys("this string is a bit longer so it should probably take a longer time to type out maybe") == 5075);
    }

    public int alternatingKeys(final String t) {
        if ("".equals(t)) {
            return 0;
        }
        int s = 0;
        final String L = "qwertasdfgzxcvb";
        Boolean p = null;
        for (final String c : t.split("")) {
            if (null != p) {
                if (" ".equals(c)) {
                    s += 50;
                    p = !p;
                } else {
                    final boolean h = L.contains(c);
                    if ((h && p) || (!h && !p)) {
                        s += 75;
                    } else {
                        s += 50;
                        p = h;
                    }
                }
            } else {
                s += 50;
                if (!" ".equals(c)) {
                    p = L.contains(c);
                }
            }
        }

        return s;
    }

}
