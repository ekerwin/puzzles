package com.kingofnerds.puzzles.codefight;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * // @formatter:off
 * The difference between two sequences of the same length a1, a2, a3,..., an
 * and b1, b2, b3,..., bn can be defined as the sum of absolute differences
 * between their respective elements:
 *
 * diff(a, b) = |a1 - b1| + |a2 - b2| + ... + |an - bn|.
 *
 * For the given sequences a and b (not necessarily having the same lengths)
 * find a subsequence b' of b such that diff(a, b') is minimal. Return this
 * difference.
 *
 * Example
 *
 * For a = [1, 2, 6] and b = [0, 1, 3, 4, 5], the output should be
 * closestSequence2(a, b) = 2.
 *
 * The best subsequence will be b' = [1, 3, 5] which has a difference of 2 with
 * a.
 *
 * [time limit] 3000ms (java)
 * [input] array.integer a
 * [input] array.integer b
 * [output] integer
 *
 * Constraints:
 * 3 ≤ a.length ≤ 1000
 * -1000 ≤ a[i] ≤ 1000
 * a.length ≤ b.length ≤ 1000
 * -1000 ≤ b[i] ≤ 1000
 *
 * // @formatter:on
 */
public class ClosestSequence {
    public static void main(final String[] args) {
        // 4
        final int[] a = new int[] { 1, 3, 5 };
        final int[] b = new int[] { 5, 3, 1, 5, 3, 0 };
        final ClosestSequence closestSequence = new ClosestSequence();
        final int difference = closestSequence.diff(a, b);
        System.out.println(difference);

        // 7
        final int[] a2 = new int[] { 1, 2, 1, 2, 1, 2 };
        final int[] b2 = new int[] { 3, 0, 0, 3, 0, 3, 3, 0, 0 };
        final int difference2 = closestSequence.diff(a2, b2);
        System.out.println(difference2);
    }

    public int diff(final int[] a, final int[] b) {
        final Set<List<Integer>> subsets = findSubsets(new ArrayList<>(), b, a.length);
        int minimumDifference = Integer.MAX_VALUE;
        for (final List<Integer> subset : subsets) {
            minimumDifference = Math.min(minimumDifference, differenceOfArrays(a, subset));
        }
        return minimumDifference;
    }

    public Set<List<Integer>> findSubsets(final List<Integer> currentSubset, final int[] array, final int targetSize) {
        final Set<List<Integer>> subsets = new HashSet<>();
        for (int index = 0; index < array.length; index++) {
            final List<Integer> increasedSubset = new ArrayList<>();
            increasedSubset.addAll(currentSubset);
            increasedSubset.add(array[index]);
            if (increasedSubset.size() == targetSize) {
                subsets.add(increasedSubset);
            } else {
                subsets.addAll(findSubsets(increasedSubset, Arrays.copyOfRange(array, index + 1, array.length), targetSize));
            }
        }

        return subsets;
    }

    // a.size == b.size
    private int differenceOfArrays(final int[] a, final List<Integer> b) {
        int difference = 0;
        for (int index = 0; index < a.length; index++) {
            difference += Math.abs(a[index] - b.get(index));
        }
        return difference;
    }

}
