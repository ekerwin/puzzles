package com.kingofnerds.puzzles.peggame;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import com.kingofnerds.puzzles.peggame.solution.EightPegsLeftNoLegalMovesSolution;
import com.kingofnerds.puzzles.peggame.solution.PegGameSolution;

public class PegGameTest {
    private final PegGameMoves pegGameMoves = new PegGameMoves();

    @Test
    public void testPegGameDisplay() {
        final Set<Integer> topPegMissing = new HashSet<>();
        topPegMissing.add(1);
        PegGameBoard board = new PegGameBoard(pegGameMoves);
        board = board.removePegs(topPegMissing);
        System.out.println(board.toString());
        for (final PegGameMove move : board.getLegalMoves()) {
            System.out.println(move);
        }
    }

    @Test
    public void testPegGameMove() {
        final Set<Integer> topPegMissing = new HashSet<>();
        topPegMissing.add(1);
        PegGameBoard board = new PegGameBoard(pegGameMoves);
        board = board.removePegs(topPegMissing);
        board = board.movePegs(new PegGameMove(4, 2, 1));
        System.out.println(board.toString());
        for (final PegGameMove move : board.getLegalMoves()) {
            System.out.println(move);
        }
    }

    @Test
    public void testFindingSinglePegSolution() {
        final Set<Integer> topPegMissing = new HashSet<>();
        topPegMissing.add(1);
        PegGameBoard board = new PegGameBoard(pegGameMoves);
        board = board.removePegs(topPegMissing);

        final PegGameSolution solution = new EightPegsLeftNoLegalMovesSolution();
        final PegGameSolutionFinder solutionFinder = new PegGameSolutionFinder();
        final List<PegGameState> pegGameStates = solutionFinder.solve(board, solution);
        for (final PegGameState pegGameState : pegGameStates) {
            System.out.println(pegGameState);
        }
    }

}
